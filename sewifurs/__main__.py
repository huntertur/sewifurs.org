from . import link_bot
from . import main_bot
from . import website

website.init()
main_bot.init()
link_bot.init()
