from configparser import ConfigParser
from dataclasses import dataclass


@dataclass
class Chat:
    '''A chatroom listed in the chat directory.'''
    name: str
    minage: int | None
    image: str
    link: str
    members: int
    region: str
    text: str


config = ConfigParser()
config.read('chats.ini')
chats: dict[str, list[Chat]] = {
    'Milwaukee': [],
    'Madison': [],
    'Wisconsin': [],
    'Chicago': [],
}
for section in config.sections():
    chat = Chat(
        name=section,
        minage=config.getint(section, 'minage', fallback=None),
        image=config.get(section, 'image'),
        link=config.get(section, 'link'),
        members=config.getint(section, 'members', fallback=0),
        region=config.get(section, 'region'),
        text=config.get(section, 'text'),
    )
    chats[chat.region].append(chat)
for region in chats.values():
    region.sort(key=lambda chat: (chat.members, chat.name), reverse=True)
