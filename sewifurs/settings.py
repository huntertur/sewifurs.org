'''Application-specific wrapper for flask.Config.'''
from flask import Config

from .dns import DnsEditor


class Settings:
    '''Wrapper for flask.Config that sets defaults and enforces requirements.'''

    required = (
        'ADMIN_CHAT_ID',
        'BOT_CONTACT',
        'BOT_TOKEN',
    )

    def __init__(self, app_config: Config):
        for key in self.required:
            if key not in app_config:
                raise KeyError(f'required setting not in config: {key}')

        self.app_config = app_config

    @property
    def admin_chat_id(self) -> int:
        '''ID of the Telegram chat the bot should listen to admin commands in.'''
        return self.app_config['ADMIN_CHAT_ID']

    @property
    def allowed_channels(self) -> list[int]:
        '''IDs of the Telegram chats the bot should record news messages from.'''
        return self.app_config.get('ALLOWED_CHANNELS', [])

    @property
    def base(self) -> str:
        '''Base URL of the website.'''
        return self.app_config.get('BASE', 'http://127.0.0.1:5000')

    @property
    def bluesky_domains(self) -> dict[str, DnsEditor]:
        '''Domains configured for self-serve Bluesky custom handles.'''
        return self.app_config.get('BLUESKY_DOMAINS', {})

    @property
    def bot_contact(self) -> str:
        '''Public handle of the channel to send updates to.'''
        return self.app_config['BOT_CONTACT']

    @property
    def bot_token(self) -> str:
        '''API token for the Telegram bot to login with.'''
        return self.app_config['BOT_TOKEN']

    @property
    def client_ip(self) -> str:
        '''Client IP used for Namecheap API requests.'''
        return self.app_config.get('CLIENT_IP', '127.0.0.1')

    @property
    def development(self) -> str | None:
        '''Which development instance this is, if set; if set to None, production.'''
        return self.app_config.get('DEVELOPMENT', 'local')

    @property
    def mappoll_base(self) -> str:
        '''Base URL of the PollWhere service.'''
        return self.app_config.get('MAPPOLL_BASE', 'https://pollwhere.com')

    @property
    def show_news(self) -> bool:
        '''Whether to display news messages on the homepage.'''
        return self.app_config.get('SHOW_NEWS', False)
