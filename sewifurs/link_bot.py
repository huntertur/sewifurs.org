'''Periodic link checker.'''
import logging
from threading import Thread
import time
from urllib.error import URLError
from urllib.request import Request, urlopen

from telebot import TeleBot

from .chats import Chat, chats
from .website import settings

logger = logging.getLogger(__name__)

link_bot = TeleBot(settings.bot_token)


def link_check_loop():
    while True:
        logger.info('Checking chat links...')
        broken_chats: list[tuple[Chat, str]] = []
        for region in chats.values():
            for chat in region:
                try:
                    # Discord doesn't like urllib's user agent
                    request = Request(chat.link, headers={'User-Agent': 'curl/7.81.0'})
                    with urlopen(request):
                        # unsuccessful statuses raise an exception
                        pass
                except URLError as e:
                    broken_chats.append((chat, e.reason))
        logger.info('%d broken chat link(s) found', len(broken_chats))
        if broken_chats:
            message = 'The following chat links are broken:'
            for chat, reason in broken_chats:
                message += f'\n{chat.name} - {chat.link} - {reason}'
            link_bot.send_message(settings.bot_contact, message)
        time.sleep(3600)


def init():
    Thread(target=link_check_loop, name='LinkBot').start()
