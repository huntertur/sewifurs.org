'''The main, command-reading Telegram bot portion of the application.'''
import logging
from threading import Thread

from sqlalchemy.exc import IntegrityError
from telebot import TeleBot
from telebot.apihelper import ApiTelegramException
from telebot.types import ChatJoinRequest, Message

from .website import app, CheckChat, db, NewsMessage, settings

logger = logging.getLogger(__name__)

main_bot = TeleBot(settings.bot_token)


@main_bot.message_handler(commands=['help'])
def command_help(message: Message):
    main_bot.reply_to(message, '''
/bluesky - Set up a custom Bluesky Social handle.
/chat_id - Get the ID for this chat.
/check_add chat_id - Add a chat to the auto-approval system.
/check_list - List all chats in the auto-approval system.
/check_remove - Remove a chat from the auto-approval system.
/help - Display this message.
/mappoll - Create a location-based poll. (Not fully implemented)
''')


def is_check_request_valid(message: Message, check_admin: bool) -> tuple[bool, int]:
    '''Make sure the preconditions for the check commands are satisfied.'''
    if message.chat.id != settings.admin_chat_id:
        main_bot.reply_to(message, 'This command cannot be used here.')
        return False, 0

    argv = message.text.split()
    if len(argv) != 2:
        main_bot.reply_to(message, 'This command takes exactly one argument.')
        return False, 0

    try:
        chat_id = int(argv[1])
    except ValueError:
        main_bot.reply_to(message, 'chat_id must be an integer.')
        return False, 0

    if check_admin:
        try:
            chat_admins = main_bot.get_chat_administrators(chat_id)
        except ApiTelegramException:
            main_bot.reply_to(message, "Could not retrieve the requested chat's admins.")
            return False, chat_id

        for chat_admin in chat_admins:
            if chat_admin.user.id == main_bot.user.id:
                break
        else:
            main_bot.reply_to(message, 'Not an admin in the requested chat.')
            return False, chat_id

        if not chat_admin.can_invite_users:
            main_bot.reply_to(message, 'Need permission to invite users in the requested chat.')
            return False, chat_id

    return True, chat_id


@main_bot.message_handler(commands=['bluesky'])
def command_bluesky(message: Message):
    if not message.from_user.username:
        main_bot.reply_to(message, 'Please set a username before using this command!')
        return

    available_domains = []
    for domain, editor in settings.bluesky_domains.items():
        for chat_id in editor.chat_ids:
            try:
                if main_bot.get_chat_member(chat_id, message.from_user.id):
                    available_domains.append(domain)
                    break
            except ApiTelegramException:
                pass

    if len(message.text.split()) != 3:
        main_bot.reply_to(message, f'''
usage: <code>/bluesky domain did</code>

<code>did</code> must be the value of <code>did</code> that Bluesky gives you \
via the Change Handle dialog.

<code>domain</code> can be any of the following, based on the chatrooms you are in:
{'\n'.join(f'- {domain}' for domain in available_domains)}
''', parse_mode='HTML')
        return

    _, domain, did = message.text.split()

    if domain not in available_domains:
        main_bot.reply_to(message, 'No chatrooms you are in are set up with that domain.')
        return

    if did.startswith('did=did:'):
        did = did[4:]

    main_bot.send_chat_action(message.chat.id, 'typing')

    editor = settings.bluesky_domains[domain]
    prefix = message.from_user.username.lower()

    try:
        editor.add_record(domain, f'_atproto.{prefix}', 'TXT', did)
    except Exception as e:
        main_bot.reply_to(message, f'⚠️ An error occurred:\n\n{e}')
        raise

    main_bot.reply_to(message,
        f'Verification record added! In a few minutes, try changing your '
        f'Bluesky handle to <code>@{prefix}.{domain}</code>.', parse_mode='HTML')


@main_bot.message_handler(commands=['chat_id'])
def command_chat_id(message: Message):
    main_bot.reply_to(message, str(message.chat.id))


@main_bot.message_handler(commands=['check_add'])
def command_check_add(message: Message):
    valid, chat_id = is_check_request_valid(message, True)
    if not valid:
        return
    title = main_bot.get_chat(chat_id).title
    check = CheckChat(chat_id=chat_id, title=title)
    try:
        with app.app_context():
            db.session.add(check)
            db.session.commit()
    except IntegrityError:
        main_bot.reply_to(message, 'That chat is already being checked.')
    else:
        main_bot.reply_to(message, f'Now checking: {title}')


@main_bot.message_handler(commands=['check_list'])
def command_check_list(message: Message):
    if message.chat.id != settings.admin_chat_id:
        main_bot.reply_to(message, 'This command cannot be used here.')
        return
    with app.app_context():
        checks: list[CheckChat] = CheckChat.query.all()
    reply = 'Currently checked chats:'
    for check in checks:
        reply += f'\n- {check.title} ({check.chat_id})'
    main_bot.reply_to(message, reply)


@main_bot.message_handler(commands=['check_remove'])
def command_check_remove(message: Message):
    valid, chat_id = is_check_request_valid(message, False)
    if not valid:
        return
    with app.app_context():
        rows = CheckChat.query.filter(CheckChat.chat_id == chat_id).delete()
        db.session.commit()
    reply = 'That chat is not checked.' if rows == 0 else 'Chat removed.'
    main_bot.reply_to(message, reply)


@main_bot.chat_join_request_handler()
def check_join_request(request: ChatJoinRequest):
    with app.app_context():
        check_chats: list[CheckChat] = CheckChat.query.all()

    for check_chat in check_chats:
        if check_chat.chat_id == request.chat.id:
            break
    else:
        # No need to announce that the bot is ignoring unchecked chats
        return

    user = request.from_user

    username = f'(@{user.username})' if user.username else '(no username)'
    message = f'Chat join request for {user.full_name} {username} ({user.id})'
    message += f' in {check_chat.title} ({check_chat.chat_id})'

    for check_chat in check_chats:
        if (member := main_bot.get_chat_member(check_chat.chat_id, user.id)) and member.status == 'member':
            if main_bot.approve_chat_join_request(request.chat.id, user.id):
                message += ' is auto-approved because the user is present in another checked chat.'
                message += f' They are present in {check_chat.title} ({check_chat.chat_id}). ✅'
            else:
                message += ' met the auto-approval criteria but is unable to be approved by the bot.'
                message += ' It is possible the join request was handled before the bot could handle it. ⚠️'
            break
    else:
        message += ' is not auto-approved because the user is not present in any checked chats. 🆕'

    main_bot.send_message(settings.admin_chat_id, message)


@main_bot.message_handler(commands=['mappoll'])
def command_mappoll(message: Message):
    main_bot.reply_to(message, f'{settings.mappoll_base}/create-poll')


def channel_message_filter(message: Message) -> bool:
    if message.chat.id not in settings.allowed_channels:
        logger.info('Ignoring post from disallowed channel id=%d', message.chat.id)
        return False
    return True


@main_bot.channel_post_handler(content_types=('text', 'photo'), func=channel_message_filter)
def record_message(message: Message):
    logger.info('Recording message with id=(%d, %d)', message.chat.id, message.id)
    news = NewsMessage(channel_id=message.chat.id, message_id=message.id,
                       date=message.date, text=message.text or message.caption)
    with app.app_context():
        db.session.add(news)
        db.session.commit()


@main_bot.edited_channel_post_handler(content_types=('text', 'photo'), func=channel_message_filter)
def update_message(message: Message):
    logger.info('Updating message with id=(%d, %d)', message.chat.id, message.id)
    with app.app_context():
        db.session.query(NewsMessage) \
                .filter(NewsMessage.channel_id == message.chat.id) \
                .filter(NewsMessage.message_id == message.id) \
                .update({'text': message.text or message.caption})
        db.session.commit()


def init():
    Thread(target=main_bot.infinity_polling, name='MainBot').start()
