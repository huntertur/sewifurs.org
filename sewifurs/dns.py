'''Things needed to edit DNS records for a domain.'''
from abc import ABC, abstractmethod
from typing import Iterable

from namecheap import Api
from namecom import Name


class DnsEditor(ABC):
    '''Interface for a DNS editor for a domain.'''
    def __init__(self, username: str, token: str, chat_ids: Iterable[int]):
        self.username = username
        self.token = token
        self.chat_ids = chat_ids

    @abstractmethod
    def login(self):
        '''Login to this DNS record-managing service.'''

    @abstractmethod
    def add_record(self, domain: str, host: str, type: str, answer: str):
        '''Add a DNS record to a domain.'''


class NamecheapEditor(DnsEditor):
    '''DNS record editor for Namecheap.'''
    def __init__(self, username, token, chat_ids):
        self.api = None
        super().__init__(username, token, chat_ids)

    def login(self):
        from .website import settings

        self.api = Api(
            ApiUser=self.username,
            ApiKey=self.token,
            ClientIP=settings.client_ip,
            sandbox=settings.development is not None,
        )

    def add_record(self, domain, host, type, answer):
        if not self.api:
            raise RuntimeError('must login before adding records')

        self.api.add_record(domain, type, answer, host)


class NamecomEditor(DnsEditor):
    '''DNS record editor for Name.com.'''
    def __init__(self, username, token, chat_ids):
        self.name = None
        super().__init__(username, token, chat_ids)

    def login(self):
        from .website import settings

        self.name = Name(
            name=self.username,
            token=self.token,
            debug=settings.development is not None,
        )

    def add_record(self, domain, host, type, answer):
        if not self.name:
            raise RuntimeError('must login before adding records')

        self.name.create_record(domain, host, type, answer)
