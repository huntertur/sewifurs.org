'''The entry point of the application.'''
from datetime import datetime
import logging
from threading import Thread
from zoneinfo import ZoneInfo

from flask import Flask, make_response, render_template
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from .chats import chats
from .settings import Settings

app = Flask(__name__, instance_relative_config=True)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config.from_pyfile('config.py')
settings = Settings(app.config)

db = SQLAlchemy(app)
migrate = Migrate(app, db)

logging.basicConfig(datefmt='%Y-%m-%dT%H:%M:%S%z',
                    format='[%(asctime)s] (%(name)s) %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

for editor in settings.bluesky_domains.values():
    editor.login()


class NewsMessage(db.Model):
    '''A news message from the SEWI Furs Telegram channel.'''
    channel_id = db.Column(db.Integer, primary_key=True)
    message_id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Integer)
    text = db.Column(db.Text)

    def __repr__(self):
        return (f'NewsMessage(channel_id={self.channel_id!r}, message_id={self.message_id!r}, '
                f'date={self.date!r}, text={self.text!r})')

    @property
    def sent_at(self) -> datetime:
        return datetime.fromtimestamp(self.date).astimezone(ZoneInfo('America/Chicago'))


class CheckChat(db.Model):
    '''A Telegram group that should be checked for auto-approval.'''
    chat_id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text)

    def __str__(self):
        return f'{self.title} ({self.chat_id})'


@app.route('/')
def index():
    news = NewsMessage.query.order_by(NewsMessage.date.desc()).limit(3)
    return render_template('index.html', chats=chats, settings=settings, news=news)


@app.route('/about')
def about():
    return render_template('about.html', settings=settings)


@app.route('/pictures')
def pictures():
    return render_template('pictures.html', settings=settings)


@app.route('/services')
def services():
    check_chats = CheckChat.query.order_by(CheckChat.title).all()
    return render_template('services.html', check_chats=check_chats, settings=settings)


@app.route('/robots.txt')
def robots_txt():
    text = render_template('robots.txt', settings=settings)
    response = make_response(text, 200)
    response.mimetype = 'text/plain'
    return response


def init():
    Thread(target=lambda: app.run(host='0.0.0.0'), name='WebServer').start()
