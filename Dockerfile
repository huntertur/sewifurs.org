FROM python:3.13.2-alpine3.21

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN apk add --no-cache build-base \
    && pip install --no-cache-dir -r requirements.txt \
    && apk del build-base

COPY . .

ENV FLASK_APP=sewifurs.website

CMD ["./docker_cmd.sh"]
