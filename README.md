This is the SEWI Furs website, hosted at https://sewifurs.org/.

Copyright (C) 2021-2025 Hunter "Lilia Roo" Turcin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

For the images in the `static/` directory, please contact the respective
chat owners for any usage permissions. Chat owners may be found in the
Chat Directory on the website. The exception is the `static/sewifurs.png`
file, which is released into the public domain; see the About section
of the website for more details.

# Setup

First, you will need to create a Telegram bot and acquire its associated token.
This bot should exist in at least one Telegram channel; do note the bot does
not need any special permissions to read the channel messages. Then, choose
one of the following methods to run the service, substituting values as needed
(use a bot like `@username_to_id_bot` to get the channel IDs).

Consider adding `DEVELOPMENT = 'staging'` to clearly indicate this is a
development deployment and setting `BASE` to the base URL for the website.
If this is a production deployment, set `DEVELOPMENT = None`.

## Container setup

With Docker installed:

```sh
docker build -t sewifurs.org .
mkdir instance
echo "ADMIN_CHAT_ID = -10012345" >> /path/to/instance/config.py
echo "BOT_CONTACT = '@SomeStatusChannel'" >> /path/to/instance/config.py
echo "BOT_TOKEN = 'your_token_here'" >> /path/to/instance/config.py
docker run -v /path/to/instance:/usr/src/app/instance -p 5000:5000 sewifurs.org
```

Then, browse to http://127.0.0.1:5000/.

## Native setup

With Python 3.13 installed:

```sh
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
echo "ADMIN_CHAT_ID = -10012345" >> instance/config.py
echo "BOT_CONTACT = '@SomeStatusChannel'" >> instance/config.py
echo "BOT_TOKEN = 'your_token_here'" >> instance/config.py
export FLASK_APP=sewifurs.website
flask db upgrade
python3 -m sewifurs
```

Then, browse to http://127.0.0.1:5000/.

# Database schema changes

[Flask-Migrate](https://flask-migrate.readthedocs.io) handles migrations.

First, `export FLASK_APP=sewifurs.website`. Then, depending on what you want to
do:

* Create new migration: `flask db migrate -m 'Change thing'`
* Migrate to latest schema: `flask db upgrade`

# Editing content

There are two types of site content: news messages and chatrooms.

## Editing news messages

Invite the Telegram bot from the Setup section to one or more channels.
Then, set `ALLOWED_CHANNELS` to a `list[int]` where each element is an
ID of a channel whose messages you want to be tracked by the Telegram bot.
The three latest messages across all channels will be displayed on the website
automatically. If these messages are edited, the new content will be seen
on the website too.

Due to Telegram Bot API limitations, channel message deletions are not tracked.

News messages are only shown if `SHOW_NEWS` is `True`. If it is `False`, news
messages will still be tracked, just not shown. The default value is
currently `False`.

## Editing chat directory

The chat directory is deployed with the web application as the `chats.ini` file
present in the root of the repository. See the comments at the start of the
file to learn what the format for adding a chatroom is. My recommendation is to
round member counts to the nearest multiple of five.

An image representing the chatroom should also be placed in the `static`
directory.

On startup and every 60 minutes after, the bot will check the validity of all
chat directory links and will send a message in the `BOT_CONTACT` channel if
a broken link is found.

# Auto-approval

The bot may be configured to auto-approve people who request to join 'checked'
groups if they are already present in at least one other 'checked' group.
Add the bot as an admin in the groups you wish to enable auto-approval
functionality in, then use the `/check_add` command in the designated admin
group to add each group to the auto-approval system. The bot must have
permission to invite users.

The reason for why the bot auto-approved or didn't auto-approve a join
request will be logged to the configured admin chat.

# Location-based polls

Set `MAPPOLL_BASE` to customize which instance of PollWhere should be used for
the `/mappoll` command. Currently, this command only pastes a link to creating
a location-based poll, but proper integration is planned for the future.

# Bluesky Social custom handles

The bot may be configured to allow people to self-serve custom subdomain
handles for Bluesky Social. Every enrolled domain has one or more associated
chatrooms that a person must be in to get access to that domain name. As of
writing, the supported registrars are Namecheap and Name.com.

```py
from sewifurs.dns import NamecheapEditor, NamecomEditor

CLIENT_IP = 'put an IPv4 address here; needed for Namecheap API'
BLUESKY_DOMAINS = {
    'furwaukee.org': NamecomEditor('username', 'token', [-12345]),
    'sewifurs.org': NamecheapEditor('username', 'token', [-12345]),
}
```

If this is not a production instance of sewifurs.org, the sandbox domain for
Namecheap and testing server for Name.com will automatically be used.

To add new DNS editors, implement the abstract base class `dns.DnsEditor`
and use your new class in `BLUESKY_DOMAINS`.
